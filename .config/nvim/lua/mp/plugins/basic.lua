return {
    { 'mg979/vim-visual-multi' },
    { 'tpope/vim-fugitive' },
    { 'tpope/vim-sleuth' },
    { 'j-hui/fidget.nvim', config = true },
}
